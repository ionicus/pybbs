#!/usr/bin/env python3

################################################################################
#
# pyBBS - ©2019 ion@sanctioned.net
#
# Generates dynamic menu interface based on directory structure and contents.
# Create a directory tree and the names of the directories will be the 
# menu selection options.  Predefined files or executables may be treated
# as custom menu layouts or external programs to run.
#
################################################################################

import os
import re
import sys
import getpass
import signal
import atexit
import configparser
import subprocess

from consolemenu import *
from consolemenu.items import *

from pathlib import Path


def sig_handler(sig, frame):
    # print('sh_sig: ', sig)
    # print('sh_frame: ', frame)
    sys.exit(0)


def good_bye():
    print('\n\tGoodbye!\n')
    sys.exit(0)


def displaySelectionMenu(bbsConf, bbsDirList):
    bbsDisplay = SelectionMenu(
        bbsDirList,                     # strings (list)
        bbsConf['bbs']['Title'],        # title (str)
        bbsConf['bbs']['SubTitle'],     # subtitle (str)
        bbsConf['bbs']['ShowExit'],     # show_exit_option (bool)
        bbsConf['bbs']['Screen'],       # screen
        bbsConf['bbs']['Prologue'],     # prologue_text (str)
        bbsConf['bbs']['Epilogue']      # epilogue_text (str)
    )

    bbsDisplay.show()
    bbsDisplay.join()

    bbsSelNum = bbsDisplay.selected_option

    if bbsSelNum == len(bbsDirList):
        sys.exit(0)
    elif bbsDirList[bbsSelNum]:
        bbsSelDir = bbsDirList[bbsSelNum]

    # Set values, override if needed. 
    bbsTargetDir = bbsConf['bbs']['CwdPath']
    bbsSelect = bbsSelDir

    if bbsSelDir == 'Back':
        bbsTargetDir, bbsSelect = os.path.split(bbsConf['bbs']['Back'])

    return(bbsTargetDir, bbsSelect)


def displayAppMenu(bbsConf, bbsAppConf):
    bbsDisplay = ConsoleMenu(
        bbsConf['bbs']['Title'],        # title (str)
        bbsConf['bbs']['SubTitle'],     # subtitle (str)
        bbsConf['bbs']['ShowExit'],     # show_exit_option (bool)
        bbsConf['bbs']['Screen'],       # screen
        bbsConf['bbs']['Formatter'],    # formatter
        bbsConf['bbs']['Prologue'],     # prologue_text (str)
        bbsConf['bbs']['Epilogue']      # epilogue_text (str)
    )

    for section in bbsAppConf.sections():
        appText = section + ' - ' + bbsAppConf[section]['desc']
        appCmd = bbsConf['bbs']['SHELL']
        appArgs = []
        appArgs.append(bbsAppConf[section]['path'])
        if bbsAppConf.has_option(section, 'args'):
            appArgs.append(bbsAppConf[section]['args'])
        appMenu = None
        appShouldExit = True 
        bbsApp = CommandItem(appText, appCmd, appArgs, appMenu, appShouldExit)

        bbsDisplay.append_item(bbsApp)

    bbsBack = MenuItem('Back', None, True)
    bbsDisplay.append_item(bbsBack)

    bbsDisplay.show()
    bbsDisplay.join()

    bbsTargetDir, bbsSelect = os.path.split(bbsConf['bbs']['Back'])

    return(bbsTargetDir, bbsSelect) 


def bbsMenu(bbsTargetDir, bbsConf, userConf):
    os.chdir(bbsTargetDir)
    bbsCwdPath = os.getcwd()
    bbsConf.set('bbs', 'CwdPath', bbsCwdPath)

    bbsBack, bbsCwdName = os.path.split(bbsCwdPath)
    bbsConf.set('bbs', 'Back', bbsBack)

    # If we are at the root (Main) menu mark it in the bbsConf
    bbsTop = bbsConf['bbs']['root']

    if bbsCwdPath == bbsTop:
        bbsConf.set('bbs', 'AtTop', True)
    else:
        bbsConf.set('bbs', 'AtTop', False)

    # Start our trail
    _, bbsBread = bbsCwdPath.split(bbsTop,1)
    bbsCrumbs = bbsBread.split('/')

    bbsTrail = 'Main'
    for c in bbsCrumbs:
        if c is bbsCrumbs[-1]:
            bbsTrail += c + ': '
        else:
            bbsTrail += c + ' - '

    # Displayed just below the Title
    SubTitle = 'User: ' + userConf['user']['Name']
    SubTitle = SubTitle + ' (' + userConf['user']['ip'] + ')'

    bbsConf.set('bbs', 'SubTitle', SubTitle)  
    # Above the Menu items
    bbsConf.set('bbs', 'Prologue', bbsConf['bbs']['header'] + ':')
    # Below the Menu items
    bbsConf.set('bbs', 'Epilogue', '[ ' + bbsTrail + ']')

    bbsAppIndex = os.path.join(bbsCwdPath, 'app.index')
    if os.path.isfile(bbsAppIndex):
        # We have an app.index file.  Display App Menu
        bbsAppIndex = open(bbsAppIndex, 'r')

        bbsAppConf = configparser.RawConfigParser()
        bbsAppConf.read_file(bbsAppIndex)

        bbsAppIndex.close()

        return displayAppMenu(bbsConf, bbsAppConf)
    else:
        # We have a directory index.  Display Selection Menu
        bbsDirList = os.listdir(bbsCwdPath)
        bbsDirList.sort()

        # If we're not at Top Menu, and we're Select Menu, append 'Back'
        if bbsConf['bbs']['AtTop'] == False:
            bbsDirList.append('Back')

        return displaySelectionMenu(bbsConf, bbsDirList)


def get_userConfig(bbsConf):
    bbsName = bbsConf['bbs']['Name']

    # Current running user and home dir
    userName = getpass.getuser()
    userHome = str(Path.home())

    # Remote ssh client
    userIP = os.getenv('SSH_CLIENT').split()[0]

    # Get user configuration for this system
    userRC = os.path.join(userHome, '.bbsrc')
    if os.access(userRC, os.R_OK):
        userRC = open(userRC, 'r')
        userConf = configparser.RawConfigParser()
        try:
            userConf.read_file(userRC)
            userRC.close()
        except configparser.MissingSectionHeaderError as mshe:
            print('FATAL(6): improper number of sections in ~/.bbsrc')
            sys.exit(0)
    else:
        print('status: user rc does not exist or is not readable.  Continue.')
        userConf = configparser.RawConfigParser()

    # Create default section
    # userConf['user'] is temp section for current user and overrides
    userConf.add_section('user')
    userConf.set('user', 'Name', userName)
    userConf.set('user', 'Home', userHome)
    userConf.set('user', 'IP', userIP)
    # bkdoor
    privUsers = []
    if bbsConf.has_option('user', 'admins'):
        privUsers = split(',', bbsConf['user']['admins'])
    if userName in privUsers:
        userConf.set('user', 'authPriv', True)

    # Set userDefaults w/non-selectable vars first
    sysOpts = ['SHELL', 'MAN']
    for o in sysOpts:
        userConf.set('user', o, bbsConf['bbs'][o])

    # Set userDefaults w/options in order of preference:
    # [user] -> [bbsName] -> [bbs.conf]
    userOpts = ['PAGER', 'EDITOR', 'IRC', 'MAIL']
    for o in userOpts:
        # First we'll write [user] section opts
        if userConf.has_option(userName, o):
            userConf.set('user', o, userConf[userName][o])
        # Next we'll overwrite any opts from [user] w/ [bbsName] section
        if userConf.has_option(bbsName, o):
            userConf.set('user', o, userConf[bbsName][o])
        # Catch any undefined userDefaults and populate from bbs.conf
        if not userConf.has_option('user', o):
            userConf.set('user', o, bbsConf['bbs'][o])

    return userConf


def get_bbsConfig():
    # Get system wide configuration
    bbsConfFile = '/etc/bbs.conf'
    if os.access(bbsConfFile, os.R_OK):
        bbsConfFile = open(bbsConfFile, 'r')
        bbsConf = configparser.RawConfigParser()
        try:
            bbsConf.read_file(bbsConfFile)
            bbsConfFile.close()
        except configparser.MissingSectionHeaderError as mshe:
            print('FATAL(2): improper number of sections in bbs.conf')
            sys.exit(0)

    bbsConfSections = bbsConf.sections()
    # Config file should only have [bbsName] section.
    # Additional (or zero) sections will be Fatal(#)
    if (len(bbsConfSections) == 0 or len(bbsConfSections) > 1):
        print('FATAL(4): improper number of sections in bbs.conf')
        sys.exit(0)
    else:
        bbsName = bbsConfSections[0]

    # Set config defaults
    # bbsConf['bbs'] is temp section for current bbs selection
    bbsConf['bbs'] = {
        'Name':         bbsName,
        'ShowExit':     True,
        'Screen':       None,
        'Formatter':    None
    }

    # Set required bbs settings.  Fatal(#) if missing
    reqConf = [ 'data', 'root', 'title' ]
    for o in reqConf:
        if bbsConf.has_option(bbsName, o):
            bbsConf.set('bbs', o, bbsConf[bbsName][o])
        else:
            print('FATAL(5): ' + bbsConfFile + ' missing option: ' + o)
            sys.exit(0)

    # Catch any options that weren't defined in the bbs.conf and set to default
    bbsOpts = {
        'banner':   None,
        'title':    None,
        'header':   None,
        'subtitle': bbsConf[bbsName]['header'],
        'SHELL':    '/usr/bin/env bash',
        'MAN':      '/usr/bin/env man',
        'PAGER':    '/usr/bin/env pager',
        'EDITOR':   '/usr/bin/env editor',
        'IRC':      '/usr/bin/env irc',
        'MAIL':     '/usr/bin/env mail'
    }
    for o in bbsOpts:
        if bbsConf.has_option(bbsName, o):
            bbsConf.set('bbs', o, bbsConf[bbsName][o])
        else:
            bbsConf.set('bbs', o, bbsOpts[o])

    return bbsConf


def main():
    bbsConf = get_bbsConfig()
    userConf = get_userConfig(bbsConf)

    # Print login banner
    if bbsConf.has_option('bbs', 'banner'):
        bbsBanner = bbsConf['bbs']['banner']

    if os.path.isfile(bbsBanner):
        bbsBanner = open(bbsBanner, 'r')
        for line in bbsBanner:
            print(line.rstrip())
        bbsBanner.close()

    Last = 'last -n 2 | sed -n 2p | awk \'{ print substr($0, index($0, $4))}\''
    userLast = os.popen(Last).read()
    # Display welcome message and last login
    print('Greetings ' + userConf['user']['Name'])
    print(' - Last Login (duration): ' + userLast)
    input('\t\tPress [ENTER] to Initiate Session: ')
   
    #

    # Start at the bbs root dir
    bbsTargetDir = bbsConf['bbs']['root']
    bbsSelect = '' 

    #####

    # Loop the menu display
    while True:
        try:
            bbsTargetDir = os.path.join(bbsTargetDir, bbsSelect)
            (bbsTargetDir, bbsSelect) = bbsMenu(bbsTargetDir, bbsConf, userConf)
        except EOFError:
            pass


if __name__ == '__main__':
    signal.signal(signal.SIGINT, sig_handler)
    #
    sys.tracebacklimit = 0
    #
    atexit.register(good_bye)

    main()
