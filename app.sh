#!/usr/bin/env bash

# External commands like games exit with info on the screen.  Menu display
# redraws the screen and overwrites so we pause and wait for input to continue.
# read() is a bash built-in so we catch all external calls with this wrapper
# to ensure we have bash and compat w/ this capability

$@ && echo -e '\n' && read -p 'press [ENTER] to continue: ' 
