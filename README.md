## pyBBS

### Description:
    Generates dynamic menu interface based on directory structure and
    contents. Create a directory tree and the names of the directories will
    be the menu selection options.  Predefined files or executables may be
    treated as custom menu layouts or external programs to run.

### Installation:
    Requires console-menu (https://pypi.org/project/console-menu/)
      # pip install console-menu

    Create top level folder for BBS tree(s):
      # mkdir -p /srv/bbs

    Create folder with name of BBS tree:
      # mkdir /srv/bbs/myBBS

    Create /etc/bbs.conf:
      # touch /etc/bbs.conf

    Edit bbs.conf section [myBBS]:
      # Must have a single section [BBS Name]
      # Currently multiple sections are fatal.
      # Handling for forked BBS instances will come in a future version.
      [myBBS]
      data: /srv/bbs/myBBS
      root: /srv/bbs/myBBS/Main
      banner: /srv/bbs/myBBS/issue.net
      title: [ myBBS ]
      header: User Interface

    Create case sensitive directory tree beneath top dir(*):
      # cd /srv/bbs/myBBS
      # mkdir Main
      # mkdir Main/DirX 
      # mkdir Main/DirY

      (*) Directories can be in different trees w/ advanced config or menus
          may be composed entirely of external handlers.  (see index.bbs)

    Create 'index.bbs' in each directory to define menu items and handlers.
      # See comments in sample index.bbs for usage and defaults. 

    Install to /usr/local/bin:
      # mv bbs.py /usr/local/bin
      # chmod 755 /usr/local/bin/bbs.py
      (optionl: restrict execution by group)

    Run from console or as captive user login shell

    pyBBS will create and write user ~/.bbsrc files if they do not exist.
    Users override some defaults with selectable options.

### Issues:
    TODO:
      Launch apps from dir contents
      Custom menus from dir contents

### License:
    No license is granted, assumed, or implied.
    
    Nor warranty
    
    © 2019
    ion@sanctioned.net
